from django.db import models

# Create your models here.

class Crypto(models.Model):
    name = models.CharField(max_length=120, null=True, blank=True, default=None)
    abbreviation = models.CharField(max_length=10, null=True, blank=True, default=None)
    unit = models.CharField(max_length=80, null=True, blank=True, default=None)
    #1whole unit = 10^{unit_power}*unit
    unit_power = models.IntegerField(default=0, blank=True, null=True)

class Individual(models.Model):
    email = models.EmailField(null=True, blank=True, default=None)
    twitter = models.CharField(max_length=100, null=True, blank=True)
    github = models.CharField(max_length=100, null=True, blank=True)
    gravatar_hash = models.CharField(max_length=32, null=True, blank=True)

    anon_mode = models.BooleanField(default=False)

    @proprty
    def total_donate_amount():
        pass

class DonateRecord(models.Model):
    individual = models.ForeignKey(Individual, on_delete=models.DO_NOTHING, related_name='donates')
    crypto = models.ForeignKey(Crypto, on_delete=models.DO_NOTHING, related_name='donates')

    #based on related crypto's unit
    amount = models.IntegerField(blank=True, null=True, default=0)
    address = models.CharField(max_length=400, null=True, blank=True)

class GiveawayRecord(models.Model):
    crypto = models.ForeignKey(Crypto, on_delete=models.DO_NOTHING, related_name='giveaway')
    amount = models.IntegerField()
    address = models.CharField()

    receiver_email = models.CharField()
    email_confirmed = models.BooleanField()
    confirmation_token = models.CharField()
