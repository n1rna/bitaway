
const urls = {
  test: {
    users: () => 'https://randomuser.me/api/',
  },
  frontend: {
    home: () => '/',
    donate: () => '/donate/',
    giveaway: () => '/giveaway/',
  }
}

export default urls;
