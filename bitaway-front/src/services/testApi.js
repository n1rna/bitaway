import urls from './urls';
import axios from 'axios';

export default class TestApi {
  static async retrieveUsers(number_of_users) {
    let i = 0;
    let result = []
    for (i=0; i<number_of_users; i++) {
      try {
        let response = await axios({
          url: urls.test.users(),
          method: 'GET',
        });
        result.push(response.data);
      } catch (error) {
        console.log(error);
      }
    }
    return result;
  }
}
