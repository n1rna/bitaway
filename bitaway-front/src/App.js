import React, { Component } from 'react';
import { BrowserRouter, Route, Switch } from 'react-router-dom';
import { Provider } from 'mobx-react';
import { Col, Row } from 'reactstrap'

import HomePageView from './views/HomePageView';
import DonateView from './views/DonateView';
import GiveawayView from './views/GiveawayView';

import Header from './components/common/Header';

import urls from './services/urls';
import store from './stores/Store';

import './App.css';

class App extends Component {
  render() {
    console.log('jjj');
    return (
      <Provider
        store={store}
      >
        <BrowserRouter>
          <div className="App">
            <Header />
            <Row className="container">
              <Col md={12}>
                <div className="content">
                  <Switch>
                    <Route path={urls.frontend.home()} exact component={HomePageView} />
                    <Route path={urls.frontend.donate()} exact component={DonateView} />
                    <Route path={urls.frontend.giveaway()} exact component={GiveawayView} />
                  </Switch>
                </div>
              </Col>
            </Row>
          </div>
        </BrowserRouter>
      </Provider>
    );
  }
}

export default App;
