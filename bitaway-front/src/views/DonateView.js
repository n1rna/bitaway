import React, { Component } from 'react';
import { Row, Col } from 'reactstrap';

import DonationTopBanner from '../components/donate/DonationTopBanner';
import DonationInformation from '../components/donate/DonationInformation';

export default class DonateView extends Component {
  render () {
    return (
      <div className="App" dir="rtl" >
      <DonationTopBanner />
      <DonationInformation />

      </div>
    );
  }
}
