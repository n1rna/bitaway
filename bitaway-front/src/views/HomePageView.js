import React, { Component } from 'react';
import { Container } from 'reactstrap';

import FirstPageBanner from '../components/home/FirstPageBanner';
import Information from '../components/home/Information';
import DonationsGravatars from '../components/home/DonationsGravatars';

export default class HomePageView extends Component {
  render () {
    return (
      <div>
        <Container>
          <FirstPageBanner />
          <Information />
          <DonationsGravatars />
        </Container>
      </div>
    );
  }
}
