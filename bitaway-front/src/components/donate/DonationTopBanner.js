import React, { Component } from 'react';
import { Row, Col, Button, Jumbotron, Badge } from 'reactstrap';

export default class DonationTopBanner extends Component {
  render () {
    return (
      <Row>
        <Col md={12}>
          <Jumbotron>
            <h1 className="display-3">Donate!</h1>
            <p className="lead">Thanks for considering to donate. You are doing a very big help to make people who are unfamiliar with the whole concept of crypto, experience their first transaction.</p>
          </Jumbotron>
        </Col>
      </Row>
    );
  }
}
