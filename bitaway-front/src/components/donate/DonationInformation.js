import React, { Component } from 'react';
import { Row, Col, Button, Jumbotron, Badge, Label, Input } from 'reactstrap';

export default class DonationInformation extends Component {
  render () {
    return (
      <Row>
        <Col md={12}>
          <Label check>
            {'I want to donate fully anonymous (providing no kinds of information about myself except my wallet address)'}
            <Input type="checkbox" />{' '}

          </Label>
        </Col>
      </Row>
    );
  }
}
