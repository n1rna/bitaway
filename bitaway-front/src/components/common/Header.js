import React, { Component } from 'react';
import { Navbar, NavItem, Nav, NavbarBrand, NavLink } from 'reactstrap';

import urls from '../../services/urls';

export default class Header extends Component {
  render () {
    console.log('fjfjfjf');
    return (
      <div>
        <Navbar color="light" light expand="md">
          <NavbarBrand href="/">BITAWAY</NavbarBrand>
          <Nav className="ml-auto" navbar>
              <NavItem>
                <NavLink href={urls.frontend.home()}>Home</NavLink>
              </NavItem>
              <NavItem>
                <NavLink href={urls.frontend.donate()}>Donate</NavLink>
              </NavItem>
              <NavItem>
                <NavLink href={urls.frontend.giveaway()}>Giveaway</NavLink>
              </NavItem>
            </Nav>
        </Navbar>
      </div>
    );
  }
}
