import React, { Component } from 'react';
import PreloaderIcon from 'react-preloader-icon';
import Puff from 'react-preloader-icon/loaders/Puff';

export default class Preloader extends Component {
  render () {
    return (
      <div style={{display: "inline-block"}}>
        <PreloaderIcon
          loader={Puff}
          size={60}
          strokeWidth={8}
          strokeColor="#000000"
          duration={800}
        />
      </div>
    );
  }
}
