import React, { Component } from 'react';
import { Row, Col, Alert, Progress } from 'reactstrap';

export default class Information extends Component {
  render () {
    return (
      <Row>
        <Col md={12}>
          <Alert color="warning">
            We are trying to reach an sufficient balance so that we can afford public giveaways.
          </Alert>
          <div className="text-center">3.56 / 10 BTC</div>
          <Progress value={40} />
          Our Bitcoin Address : 1F1tAaz5x1HUXrCNLbtMDqcw6o5GNn4xqX
          <a href="#transactions"> See transactions </a>
        </Col>
      </Row>
    );
  }
}
