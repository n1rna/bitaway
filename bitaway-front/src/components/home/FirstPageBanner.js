import React, { Component } from 'react';
import { Row, Col, Button, Jumbotron, Badge } from 'reactstrap';

export default class FirstPageBanner extends Component {
  render () {
    return (
      <Row>
        <Col md={12}>
          <Jumbotron>
            <Badge color="secondary">Beta!</Badge><h1 className="display-3">Bit-Away</h1>
            <p className="lead">Are you new to cryptocurrencies? You can have your first transaction here. Yes, we are giving away free crypto here!! xDD</p>
            <hr className="my-2" />
            <p>If you feel supportive and want to help newbies to experience their first transaction, please donate :)</p>
            <p className="lead">
            <Button color="primary">Get Bitcoin</Button>
            {'    '}
            <Button color="primary">Donate</Button>
            </p>
          </Jumbotron>
        </Col>
      </Row>
    );
  }
}
