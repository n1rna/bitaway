import React, { Component } from 'react';
import { observer, inject } from 'mobx-react';
import { Row, Col } from 'reactstrap';
import Avatar from 'react-avatar';

import Preloader from '../common/Preloader';

@inject('store')
@observer
export default class DonationsGravatars extends Component {
  constructor(props) {
    super(props);
    this.state = {
      users_data: null,
    }
    this._renderGravatars = this._renderGravatars.bind(this);
  }

  componentDidMount () {
    const {store} = this.props;
    store.retrieveUsers();
  }

  _renderGravatars() {
    const {store} = this.props;
    if (!store.users.length) {
      return (
        <Col md={12}>
          <Preloader />
        </Col>
      );
    } else {
      return store.users.map((user, idx) => {
        return (
          <Col md={1}>
            <Avatar className="donate-avatar" name={user.results[0].email} src={user.results[0].picture.medium} size="50px" />
          </Col>
        );
      });
    }

  }

  render () {
    return (
      <Row>
        <Col md={12}>
          <hr/>
          <h2>Donations</h2>
          <Row>
            {this._renderGravatars()}
          </Row>
        </Col>
      </Row>
    );
  }
}
