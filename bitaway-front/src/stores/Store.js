import { observable, action } from 'mobx';

import TestApi from '../services/testApi';

class Store {

  @observable users = [];

  @action
  async retrieveUsers () {
    const res = await TestApi.retrieveUsers(48);
    if (res) {
      this.users = res;
      console.log('im hereee', res[0].results);
    }
  }
}

const store = new Store();
export default store;
